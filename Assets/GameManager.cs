﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Text rescuedSoldiers;
    public Text soldiersInHeli;
    public Text life;

    public Soldier[] soldiers;

    public Hospital hospital;

    public Player player;

    public bool isGameOver;

    // Start is called before the first frame update
    void Start()
    {
        isGameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        rescuedSoldiers.text = "Rescued Soldiers: " + hospital.soldiersInHospital;
        soldiersInHeli.text = "Soldiers in Helicopter: " + player.soldiersInHeli;
        life.text = "Life: " + player.life;

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(2);

        if (Input.GetKeyDown(KeyCode.Q))
            SceneManager.LoadScene(0);

        if (isGameOver == true)
            return;

        if (player.life <= 0) {
            isGameOver = true;
            SceneManager.LoadScene(3);
        }

        for(int i = 0; i < soldiers.Length; i++) {
            if((soldiers[i].isRescued == true) && (hospital.soldiersInHospital >= soldiers.Length)) {
                isGameOver = true;
                SceneManager.LoadScene(4);
            }
        }

    }
}
