﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
            SceneManager.LoadScene(1);

        if (Input.GetKeyDown(KeyCode.S))
            SceneManager.LoadScene(2);

        if (Input.GetKeyDown(KeyCode.Q))
            SceneManager.LoadScene(0);

        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(2);
    }
}
