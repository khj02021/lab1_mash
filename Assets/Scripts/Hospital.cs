﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hospital : MonoBehaviour
{
    public int soldiersInHospital = 0;

    private void OnTriggerEnter2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();

        soldiersInHospital += player.soldiersInHeli;
        player.soldiersInHeli = 0;
    }
}
