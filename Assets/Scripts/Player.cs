﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 1.5f;

    public int soldiersInHeli = 0;

    public int life = 100;

    Rigidbody2D rb2d;

    AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        rb2d.AddForce(movement * speed);

        transform.rotation = new Quaternion(0, 0, 0, 0);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Soldier soldier = collision.GetComponent<Soldier>();

        if((collision.gameObject.tag == "Soldier") && (soldiersInHeli < 3)) {
            audioSource.Play();
            soldier.RescueSoldier();
        }
    }
}
