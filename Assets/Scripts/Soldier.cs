﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier : MonoBehaviour
{
    public int injuredSoldier = 1;
    public bool isRescued = false;

    public void RescueSoldier() {
        Player player = FindObjectOfType<Player>();
        if (player.soldiersInHeli < 3) {
            player.soldiersInHeli += injuredSoldier;
            isRescued = true;

            Destroy(gameObject);
        }
    }
}
