﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public int damage = 30;

    public AudioSource obstacleSound;

    private void OnTriggerEnter2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();

        if(player.life > 0) {
            player.life -= damage;
            obstacleSound.Play();
        }
    }
}
