﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int damage;

    private float upMax = 3.0f;
    private float downMax = -3.0f;
    private float currentPosition;
    private float speed = 3.0f;

    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        currentPosition = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        currentPosition += speed * Time.deltaTime;
        if(currentPosition >= upMax) {
            speed *= -1;
            currentPosition = upMax;
        }
        else if (currentPosition <= downMax) {
            speed *= -1;
            currentPosition = downMax;
        }

        transform.position = new Vector2(-3.5f, currentPosition);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        Player player = collision.GetComponent<Player>();

        if(player.life > 0) {
            player.life -= damage;
            audioSource.Play();
        }
    }
}
